#include "public/jet/sort.h"

#include <stdlib.h>

int
bubblesort(int * const   list,
           size_t const  length)
{
    /* iterate over indexes of the array */
    for ( int i = 0; i < length; ++i ) {
        /* iterate backwards from the current index to the start, swapping pairs as needed */
        for ( int j = i; j > 0; --j ) {
            if ( list[j] < list[j - 1] ) {
                int tmp = list[j - 1];
                list[j - 1] = list[j];
                list[j] = tmp;
            }
        }
    }
}
