#include "public/jet/sort.h"

#include <errno.h>
#include <stdlib.h>

/**
 * mergsort_helper()
 *
 *   This is a static helper function to mergesort(). It is a recursive function that performs the
 *   majority of the sorting work.
 *
 *   @param[in]      start     the starting index of the current sublist
 *   @param[in]      end       the ending index of the current sublist
 *   @param[in/out]  list      the list to be sorted
 *   @param[in]      scratch   the pre-allocated scratch space
 */
static void
mergesort_helper(int const    start,
                 int const    end,
                 int * const  list,
                 int * const  scratch)
{
    int length;             // length of the current sublist
    int midpoint_distance;  // midpoint distance of the current sublist
    int left_index;         // index tracker for the left half of the sublist
    int right_index;        // index tracker for the right half of the sublist

    /* base case */
    if ( start == end ) return;

    length = end - start + 1;
    midpoint_distance = length / 2;
    left_index = start;
    right_index = start + midpoint_distance;

    /* recursively sort sublists */
    mergesort_helper(start, right_index - 1, list, scratch);
    mergesort_helper(right_index, end, list, scratch);

    /* merge right and left sublists into the scratch space */
    for ( int i = start; i < start + length; i++ ) {

        /* if we've already merged in the entire left side, merge in the next value from the right
         * side */
        if ( left_index == start + midpoint_distance ) {
            scratch[i] = list[right_index++];
        }

        /* else if we've already merged in the entire right side, merge in the next value from the
         * left side */
        else if ( right_index > end ) {
            scratch[i] = list[left_index++];
        }

        /* otherwise merge in whichever value is lower */
        else {
            if ( list[left_index] < list[right_index] ) {
                scratch[i] = list[left_index++];
            } else {
                scratch[i] = list[right_index++];
            }
        }
    }

    /* write the sorted section of scratch memory back to the corresponding section of memory in
     * the original list */
    for ( int i = start; i < start + length; i++ ) {
        list[i] = scratch[i];
    }
}

/**
 * mergesort()
 *
 *   This function sorts the provided array using a merge sort in O(nlogn) time.
 *
 *   @param[in/out]  list     the array to be sorted
 *   @param[in]      length   the length of the provided array
 *
 *   @return 0 on success, errno on failure */
int
mergesort(int * const   list,
          size_t const  length)
{
    int *scratch = NULL;

    /* reset errno */
    errno = 0;

    /* allocated scratch memory equal to the lists own memory */
    scratch = (int*) malloc(length * sizeof(int));
    if ( scratch == NULL ) {
        return errno;
    }

    /* sort the list */
    mergesort_helper(0, length - 1, list, scratch);

    free(scratch);
    return 0;
}
