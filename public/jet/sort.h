#ifndef LIBJET_SORT_H_
#define LIBJET_SORT_H_

#include <stddef.h>

/**
 * bubblesort()
 *
 *   A simple implementation of the bubblesort() sorting algorithm. This function only works on
 *   integers, but could easily be adapted for use with other data types.
 *
 * @param[in/out]  list      The list to be sorted.
 * @param[in]      length    The length of the list.
 *
 * @return  0 on success, errno on failure.
 */
int
bubblesort(int * const   list,
           size_t const  length);

/**
 * mergesort()
 *
 *   A simple implementation of the mergesort() sorting algorithm. This function only works on
 *   integers, but could be easily adapted for use with other data types. This function will
 *   allocate and free it's own memory use for the operations, and will sort the list passed in.
 *
 * @param[in/out]  list      The list to be sorted.
 * @param[in]      length    The length of the list.
 *
 * @return  0 on success, errno on failure.
 */
int
mergesort(int * const   list,
          size_t const  length);

#endif // LIBJET_SORT_H_
