/**
 * Common Helper Functions for Test Programs
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define ARRAY_LENGTH(arr)  sizeof(arr) / sizeof(arr[0])

/**
 * Generates a list of random int's.
 */
int *
generate_random_values_int(size_t length)
{
    int *list = NULL;

    /* seed rng */
    srand(time(0));

    /* allocate memory for new list of values */
    list = (int*) malloc(length * sizeof(int));
    if ( list == NULL ) {
        fprintf(stderr, "failed to allocate memory for random values\n");
        return NULL;
    }

    /* get/set random values */
    for ( size_t i = 0; i < length; ++i ) {
        list[i] = rand();
    }

    return list;
}

/**
 * Verifies that a list of int's is in order.
 */
bool
in_order_int(int *   list,
             size_t  length)
{
    for ( size_t i = 0; i < length - 1; ++i ) {
        if ( list[i] > list[i + 1] ) {
            return false;
        }
    }

    return true;
}
