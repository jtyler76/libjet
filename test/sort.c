#include "common.c"

#include <jet/sort.h>

/* function pointer type for sorting algorithms */
typedef int (*sort_fct_t)(int * const, size_t);

/* enumerations of sorting algorithms */
enum {
    MERGESORT = 0,
    BUBBLESORT,
    INVALIDSORT,
};

/* test struct to hold information about a given sorting algorithm */
typedef struct sort_alg_s {
    char const * const  name;
    sort_fct_t const    alg;
} sort_alg_t;

/* list of sorting algorithms */
static const sort_alg_t sorting_algorithms[] = {
    [MERGESORT]    = { .name = "mergesort",  .alg = &mergesort  },
    [BUBBLESORT]   = { .name = "bubblesort", .alg = &bubblesort },
    [INVALIDSORT]  = NULL,
};

int
main(void)
{
    size_t test_sizes[] = { 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000 };

    /* print header */
    printf(" # of items to sort |");
    for ( int i = 0; i < INVALIDSORT; ++i ) {
        printf(" %12s |", sorting_algorithms[i].name);
    }
    printf("\n");
    printf("----------------------------------------------------------\n");

    /* iterate through list sizes, sorting with each algorithm */
    for ( size_t i = 0; i < ARRAY_LENGTH(test_sizes); ++i ) {
        /* column header */
        printf(" %18zu |", test_sizes[i]);
        fflush(stdout);

        /* sort a randomized list of this size with each algorithm */
        for ( size_t alg_idx = 0; alg_idx < INVALIDSORT; ++alg_idx ) {
            
            clock_t time;
            double duration;
            size_t list_size = test_sizes[i];
            int *list;

            list = generate_random_values_int(list_size);

            time = clock();

            sorting_algorithms[alg_idx].alg(list, list_size);

            time = clock() - time;
            duration = (double)time / CLOCKS_PER_SEC;

            if ( in_order_int(list, list_size) ) {
                printf(" %11fs |", duration);
                fflush(stdout);
            } else {
                fprintf(stderr, "SORT FAILED!\n");
                exit(EXIT_FAILURE);
            }
        }

        printf("\n----------------------------------------------------------\n");
    }

    return EXIT_SUCCESS;
}
