## makefile for libjet

libjet.so: mergesort.o bubblesort.o
	gcc -shared -o libjet.so mergesort.o bubblesort.o

mergesort.o: mergesort.c public/jet/sort.h
	gcc -fPIC -c mergesort.c

bubblesort.o: bubblesort.c public/jet/sort.h
	gcc -fPIC -c bubblesort.c

clean:
	rm -rf *.o *.so

install:
	cp libjet.so /usr/local/lib/libjet.so
	mkdir -p /usr/local/include/jet
	cp public/jet/sort.h /usr/local/include/jet/sort.h
